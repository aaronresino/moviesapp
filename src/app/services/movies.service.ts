import { Injectable } from '@angular/core';

import { Jsonp } from '@angular/http';
import { map } from 'rxjs/operators'; // Map

@Injectable({
  providedIn: 'root'
})
export class MoviesService {
  private apiKey = 'd265e511522ad7e724345d0703e7542e';
  private urlMovieDb = 'https://api.themoviedb.org/3/';

  constructor(private _jsonp: Jsonp) { }

  populars() {
    const url = `${ this.urlMovieDb }discover/movie?sort_by=popularity.desc&api_key=${ this.apiKey }&language=es&callback=JSONP_CALLBACK`;

    return this._jsonp.get(url)
    .pipe(map( response => response.json().results));
  }

  popularsKids() {
    const url = `${ this.urlMovieDb }discover/movie?certification_country=ES&certification.lte=G&sort_by=popularity.
    desc&api_key=${ this.apiKey }&language=es&callback=JSONP_CALLBACK`;

    return this._jsonp.get(url)
    .pipe(map( response => response.json().results));
  }

  search(movie: string) {
    const url = `${ this.urlMovieDb }search/movie?api_key=${ this.apiKey }&query=${ movie }&&language=es&callback=JSONP_CALLBACK`;

    return this._jsonp.get(url)
    .pipe(map(response => response.json().results));
  }

  billboard(day: string) {
    const url = `${ this.urlMovieDb }discover/movie?primary_release_date.gte=${ day }&primary_release_date.lte=${ day }
    &sort_by=popularity.desc&api_key=${ this.apiKey }&language=es&callback=JSONP_CALLBACK`;

    return this._jsonp.get(url)
    .pipe(map((response: any) => response.json().results));
  }

  movie(id: string) {
    const url = `${ this.urlMovieDb }movie/${ id }?api_key=${ this.apiKey }&language=es&callback=JSONP_CALLBACK`;

    return this._jsonp.get(url)
    .pipe(map((response: any) => response.json()));
  }
}
