import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { JsonpModule } from '@angular/http';

import {APP_ROUTING } from './app.routes';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { MovieComponent } from './components/movie/movie.component';
import { SearchComponent } from './components/search/search.component';

import { NoimagePipe } from './pipes/noimage.pipe';
import { WithimagesPipe } from './pipes/withimages.pipe';
import { MoviecardComponent } from './components/moviecard/moviecard.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    MovieComponent,
    SearchComponent,
    NoimagePipe,
    WithimagesPipe,
    MoviecardComponent
  ],
  imports: [
    BrowserModule,
    JsonpModule,
    APP_ROUTING,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
