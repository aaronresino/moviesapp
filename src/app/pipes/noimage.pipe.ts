import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'noimage'
})
export class NoimagePipe implements PipeTransform {
  URL_IMG = 'http://image.tmdb.org/t/p/w300';

  transform(image: any): string {
    if (!image) {
      return 'assets/img/noimage.png';
    } else {
      if (image) {
        return `${this.URL_IMG}${image}`;
      } else {
        return 'assets/img/noimage.png';
      }
    }
  }
}
