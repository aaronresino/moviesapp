import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'withimages'
})
export class WithimagesPipe implements PipeTransform {

  transform(value: any[]): any {
   if (value && value.length > 0) {
     return value.filter(item => item.backdrop_path !== null);
   } else {
     return [];
   }
  }
}
