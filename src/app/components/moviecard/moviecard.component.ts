import {Component, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-moviecard',
  templateUrl: './moviecard.component.html',
  styleUrls: ['./moviecard.component.css']
})
export class MoviecardComponent {
  @Input('items') items;

  constructor(private _router: Router) {}

  viewMovie(movie: any) {
    this._router.navigate(['movie', movie.id]);
  }
}
