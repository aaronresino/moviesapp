import { Component } from '@angular/core';
import { MoviesService } from '../../services/movies.service';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent {
  movies: any[] = [];
  showSearch: boolean;

  constructor(private _movieService: MoviesService, private _router: Router,
              private _activatedRoute: ActivatedRoute) {
    this._activatedRoute.params.subscribe((params: Params) => {
      if (params['movie']) {
        this.showSearch = false;
        const event = {
          target: {
            value: params['movie']
          }
        };

        this.search(event);
      } else {
        this.showSearch = true;
      }
    });
  }

  search(event: any) {
    this._movieService.search(event.target.value).subscribe(
      data => this.movies = data,
      error => console.error(error)
    );
  }

  viewMoview(id: string) {
    this._router.navigate(['movie', id]);
  }
}
