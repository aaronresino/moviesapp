import { Component } from '@angular/core';
import { MoviesService } from '../../services/movies.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  billboardMovies: any[] = [];
  popularsMovies: any[] = [];
  popularsKidsMovies: any[] = [];

  constructor(private _ms: MoviesService, private _router: Router) {
    const day = `${ new Date().getFullYear() }-${ new Date().getMonth() + 1 }-${ new Date().getDate() }`;
    this._ms.billboard(day).subscribe(
      data => {
        this.billboardMovies = data;
      },
      error => console.log(error)
    );

    this._ms.populars().subscribe(
      data => this.popularsMovies = data,
      error => console.error(error)
    );

    this._ms.popularsKids().subscribe(
      data => this.popularsKidsMovies = data,
      error => console.error(error)
    );
  }
}
