import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../../services/movies.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})
export class MovieComponent implements OnInit {
  movie: any = {};
  URL_IMG = 'http://image.tmdb.org/t/p/w500';

  constructor(private _ms: MoviesService, private _activatedRoute: ActivatedRoute,
              private _location: Location) {
    this._activatedRoute.params.subscribe((params: Params) => {
      this._ms.movie(params['id']).subscribe(
        data => {
          if (data.poster_path) {
            data.poster_path = `${this.URL_IMG + data.poster_path}`;
          } else {
            data.poster_path = 'assets/img/noimage.png';
          }

          this.movie = data;
          console.log('Movie', this.movie);
        },
        error => console.error(error)
      );
    });
  }

  back() {
    this._location.back();
  }

  ngOnInit() {
  }

}
